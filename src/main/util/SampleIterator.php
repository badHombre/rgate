<?php
namespace main\util;

class SampleIterator implements \IteratorAggregate
{
	public function getIterator()
	{
		$it = fopen("php://stdin", "r");
		if(!$it){
			throw new Exception("Invalid arguments: couldnt open stream");
		}
		while(!feof($it)){
			$str= fgetc($it);
			$str= trim($str);
			if(strlen($str)>0){
				yield $str;
			}
		}
		fclose($it);
	}
}
?>
