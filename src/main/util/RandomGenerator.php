<?php
namespace main\util;

class RandomGenerator implements \IteratorAggregate{

	private	$size=20;
	private $iteratorCount=0;
	const RANDCHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public function __construct($strSize){
		$this->size = $strSize;
	}
 
	public function getIterator(){
		while($this->iteratorCount<$this->size){
			yield substr(self::RANDCHARS,random_int(0,51),1);
			$this->iteratorCount++;       
		}
	}
}

?>
