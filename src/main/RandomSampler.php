<?php
namespace main;
use main\util\RandomGenerator;
 
class RandomSampler{

	private function sampleReservoir($streamIterator, $reservoir){
		$index = 0;
		$sampleSize = sizeof($reservoir);
		foreach ($streamIterator as $char){
			if($index<$sampleSize){
				$reservoir[$index] = $char;
			}else{
				$num = rand(0,$index);
				if($num<$sampleSize){
					$reservoir[$num] = $char;
				}
			}
			$index++;
		}
		return implode($reservoir);
	}

	public function getSample($sampleSize, $stream=null){
		$reservoir = array_fill(0,$sampleSize,0);
		if($stream==null){
			$streamIterator = new RandomGenerator($sampleSize+10);
			return $this->sampleReservoir($streamIterator, $reservoir);         
		}

		return $this->sampleReservoir($stream, $reservoir);         
	}
}
?> 
