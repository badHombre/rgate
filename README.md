# README #

Commandline random sampling using reservoir sampling algorithm.

### How do I get set up? ###

* clone repo
* cd to repo base directory
* setup composer locally if not already installed
* php composer.phar install
* cd bin
#### To generate from a given string:
* echo "test123456" | ./random-sampler 5

#### To generate from a random source within the program:
* ./random-sampler 5 generate

### To run Test cases
* cd to the base directory
* run ./vendor/bin/phpunit

### Who do I talk to? ###
Harish R(r.harish07@gmail.com)
