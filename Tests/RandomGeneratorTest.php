<?php

use main\util\RandomGenerator;

class RandomGeneratorTest extends \PHPUnit\Framework\TestCase{

	public function testCount(){
		$randGenerator = new RandomGenerator(7);
		$count = 0;
		foreach($randGenerator as $value){
			$count++;
		}
		$this->assertEquals(7,$count);
	}

	public function testRandomness(){
		$randGenerator = new RandomGenerator(7);
		$retarr = "";
		foreach($randGenerator as $value){
			$retarr = $retarr . $value;
		}
		$randGenerator = new RandomGenerator(7);
		$retarr2 = "";
		foreach($randGenerator as $value){
			$retarr2 = $retarr2 . $value;
		}
		$this->assertNotEquals($retarr,$retarr2);
	}
}
?>
