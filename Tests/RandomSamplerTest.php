<?php

use main\RandomSampler;

class RandomSamplerTest extends \PHPUnit\Framework\TestCase{

	protected $arrIt;
	protected $samplerStub;
        protected $arr;

	protected function setUp(){
                $this->arr = array("a","b","1","2","3","z","k","m","n","4");
		$this->samplerStub =  new RandomSampler();
		$this->arrIt = new ArrayIterator($this->arr);
	}

	public function testSampling(){

		$retarr =  str_split($this->samplerStub->getSample(5,$this->arrIt));
		foreach($retarr as $val){
			if(!in_array($val, $this->arr)){
				$this->assertTrue(false);
			}

		}
		$this->assertTrue(true);
	}
	
        public function testCount(){

                $retarr = $this->samplerStub->getSample(3,$this->arrIt);
                $this->assertEquals(3,strlen($retarr));
        }
        
        public function testRandomness(){

                $retarr = $this->samplerStub->getSample(2,$this->arrIt);
                $retarr2 = $this->samplerStub->getSample(2,$this->arrIt);
                $this->assertNotEquals($retarr,$retarr2);
        }
}
?>
